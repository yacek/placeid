import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DATABASES = {
    'default': {
        # Database driver
        'ENGINE': 'django.db.backends.sqlite3',
        # Replace below with Database Name if using other database engines
        'NAME': os.path.join(BASE_DIR,'db.sqlite3'),
    }
}

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
	'db',
    
)
SECRET_KEY = '6few3nci_q_o@l1dlbk81%wcxe!*6r293u629&d97!hiqat9fa'
APIKEY = '&key=AIzaSyDiSkjHCXdioRoFK1iYH0QkrTPikXlBxDs'
FILE_NAME = r'pids.txt'
PID_LIST_FILE = os.path.join(BASE_DIR,FILE_NAME) #список Place ID


