#-*- coding: utf-8 -*-
import json
import io
import re
import datetime
import time
import os
import sys
import logging

import requests
import bs4
import django
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from db.models import *
from placeid import settings

GOOGLE_API_URL = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='
isclosed = re.compile(r'Permanently closed')
logging.basicConfig(filename="placeid.log",
                    level=logging.INFO,
                    format="%(asctime)s:%(levelname)s:%(message)s")
log_delimiter_half = '-'*25
log_delimiter = '-'*80

def todate(s):
    """ Преобразование строки вида 'Tue, Aug 17' в дату """
    now = datetime.datetime.today()
    date = datetime.datetime.strptime(s,'%a, %b %d')
    date_with_year = date.replace(year=now.year)
    if date_with_year < now:
        return date_with_year.replace(year=now.year+1)
    else:
        return date_with_year

def makerequest(pid, proxy=None):
    service_args = ['--ignore-ssl-errors=true']
    apiurl = ''.join([GOOGLE_API_URL, pid.strip(), settings.APIKEY])
    apirequest = requests.get(apiurl)
    gmapurl = apirequest.json()['result']['url']
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('intl.accept_languages=en-us,en')
    if proxy:
        options.add_argument('proxy-server=%s' % proxy)
    browser = webdriver.Chrome(chrome_options=options)
    try:
        browser.get(gmapurl)
    except ConnectionError:
        browser = None
    return browser

def gethtml(browser,click=False):
    """Возвращает bs4 объект
    
    click - загружает детальные отзывы

    """
    if not browser:
        return None
    if click:
        try:
            delay=5
            rew_button = browser.find_elements_by_xpath \
                         ("//button[@class='widget-pane-link'][@vet='3648']")
            try:
                rewievs_show = WebDriverWait(browser, delay). \
                until(EC.presence_of_element_located((By.XPATH,
                 "//button[@class='widget-pane-link'][@vet='3648']")))
                rewievs_show.click()
            except TimeoutException:
                logging.error('Load rewievs timeout')
                return None
            browser.implicitly_wait(2)
            rewievs = browser.find_elements_by_xpath("//*[@vet='21866']")
            html = browser.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
            browser.quit()
        except NoSuchElementException as e:
            logging.error('Rewievs not found: %s' % e)
            browser.quit()
            return None
        return bs4.BeautifulSoup(html, 'html.parser')
    else:
        html = browser.execute_script("return document.getElementsByTagName('html')[0].outerHTML")
        return bs4.BeautifulSoup(html, 'html.parser')

def getaddress(data):
    """Возвращает адрес как строку или None"""
    address = data.find(attrs={'vet': '36622'})
    if address:
        return address.get_text()   
    else:
        return None

def getphone(data):
    """Возвращает контактный телефон или None"""
    phone = data.find(attrs={'vet': '18491'})
    if phone:
        return phone.get_text()
    else:
        return None

def getsite(data):
    """Возвращает ссылку на сайт или None"""
    website = data.find(attrs={'vet': '3443'})
    if website:
        url = website.find('a',href=True)
        if url:
            return url['href']
    return None

def getfullname(data):
    """ Возвращает полное название как строку или None"""
    name = data.find('h1',{'class': 'section-hero-header-title'})
    if name:
        return name.get_text()
    else:
        return  None
    

def gettype(data):
    """ Возвращает категорию объекта или None"""
    category = data.find('button',{'jsaction': 'pane.rating.category'})
    if category:
        return category.get_text()
    else:
        return None

def getshortrewievs(data):
    """ Возвращает массив коротких отзывов или None"""
    try:
        srewievs = data.findAll('div',{'class': 'section-review-snippet-line'})
        if srewievs:
            srews = []
            for i in srewievs:
                srews.append(i.findAll(attrs='section-review-snippet-text'))
            result = []
            for i in srews:
                result.append(''.join([text.get_text() for text in i]))
            return result
    except AttributeError as e:
        logging.error('get short rewievs, attrib error: %s' % e)
        return None

def getevents(data):
    """ Вовзращает список ближайших мероприятий или None"""
    try:
        events = data.findAll(attrs={'vet': '33716'})
        result = []
        if events:
            for i in events:
                link = i['href']
                date = todate(i.find(attrs='section-list-item-title-badge').get_text())
                name = i.find(attrs='section-list-item-subtitle').get_text()
                result.append(dict(name=name,link=link,date=date))
            return result
    except AttributeError as e:
        logging.error('get events, attrib error: %s' % e)
        return None

def getfullrewievs(data):
    """Принимает массив или None, возвращает словарь отзывов или None"""
    try:
        rew_list = data.findAll(attrs={'vet':'21866'})
        if rew_list:
            result = []
            for i in rew_list:
                author_name = i.find('div',{'class': 'section-review-title'}).get_text()
                rewievtext = i.find('span',{'class': 'section-review-text'}).get_text().replace('\n','')
                result.append(dict(author_name=author_name,rewievtext=rewievtext))
            return result
    except AttributeError as e:
        logging.error('get full rewievs, attrib error: %s' % e)
        return None

def getphoto(data):
    """ Возвращает строку со ссылкой на фотографию или None"""
    try:
        photo = data.find(attrs={'vet': '15130'})
        if photo.img:
            return photo.img['src']
        else:
            return None
    except AttributeError:
        logging.error('get photo, attrib error')
        return None

def getworktime(data):
    """ Возвращает словарь с часами работы по дням недели или None"""
    if data.find(text=isclosed):
        return None
    worktime = data.findAll(attrs={'vet': '14925'})
    if worktime:
        for i in worktime:
            day_of_week = i.findAll('th')
            work_hours = i.findAll('li')
        days = [i.get_text().strip() for i in day_of_week]
        hours = [i.get_text().strip() for i in work_hours]
        wtime = (dict(zip(days,hours)))
        return wtime
    return None

def parsebyplaceid(proxy=None):
    """ Возвращает массив словарей"""
    with io.open(settings.PID_LIST_FILE, 'r', encoding='utf-8') as file:
        pidl = file.readlines()
    Place.objects.all().delete()  
    for pid in pidl:
        if len(pid.strip()) != 27:
            continue
        time.sleep(5)
        logging.info('%s %s %s' % (log_delimiter_half,pid.strip(),log_delimiter_half))
        response = makerequest(pid, proxy)
        if response:
            try:
                data = gethtml(response)
            except ConnectionError:
                logging.error('Connection error, before click on rewievs')
                data = None
            if data:
                address = getaddress(data)
                phone = getphone(data)
                site = getsite(data)
                fname = getfullname(data)
                category = gettype(data)
                srewievs = getshortrewievs(data)
                events = getevents(data)
                photo = getphoto(data)
                worktime = getworktime(data)
                try:
                    updated_data = gethtml(response,True)
                except ConnectionError as e:
                    logging.error('Connection error, on click at rewievs')
                    updated_data = None
                frewievs = getfullrewievs(updated_data)
                error = 0
                place = Place(pid=pid,address=address,photo=photo,category=category,
                              fullname=fname,error=error,phone=phone,site=site)
                place.save()
                if srewievs:
                    for x in srewievs:
                        short_rewievs = ShortRewievs(srewiewtext=x,place=place)
                        short_rewievs.save()
                if events:
                    for x in events:
                        event = Event(place=place,**x)
                        event.save()
                if frewievs:
                    for x in frewievs:
                        full_rewievs = FullRewievs(place=place,**x)
                        full_rewievs.save()
                if worktime:
                    work_time = WorkTime(place=place,**worktime)
                    work_time.save()
            else:
                place = Place(pid=pid,error=1)
                place.save()
            logging.info(log_delimiter)

if __name__ == '__main__':
    print('Place ID parser module')

