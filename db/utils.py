#-*- coding: utf-8 -*-
import json
import io
import re
import datetime
import time
import os
import sys

import requests
import django

from db.models import *
from placeid import settings

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Language': 'en,en-US;q=0.7,en;q=0.3',
           'Accept-Encoding': 'gzip, deflate',
           'DNT': '1',
           'Connection': 'keep-alive',
           'Upgrade-Insecure-Requests': '0',
           'Cache-Control': 'max-age=0, no-cache',
           'Pragma': 'no-cache'
           }
GOOGLE_API_URL = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='

def todate(s):
    """ Преобразование строки вида 'Tue, Aug 17' в дату """
    now = datetime.datetime.today()
    date = datetime.datetime.strptime(s,'%a, %b %d')
    date_with_year = date.replace(year=now.year)
    if date_with_year < now:
        return date_with_year.replace(year=now.year+1)
    else:
        return date_with_year

def makerequest(pid, proxy=None):
    if proxy:
        prx = dict(http=''.join(['http://',proxy]))
    else:
        prx = None
    apiurl = ''.join([GOOGLE_API_URL, pid.strip(), settings.APIKEY])
    apirequest = requests.get(apiurl)
    gmapurl = apirequest.json()['result']['url']
    HEADERS.update({'Host': 'maps.google.com'})
    gmaprequest = requests.get(gmapurl, headers=HEADERS, proxies=prx,allow_redirects=False)
    if gmaprequest.status_code == 302:
        HEADERS.update({'Host': 'www.google.com'})
        gmapurl = gmaprequest.headers['Location']
        gmaprequest = requests.get(gmapurl,headers=HEADERS,proxies=prx,allow_redirects=False)
    if gmaprequest.status_code != 200:
        return None
    else:
        return gmaprequest.text

def parseresponse(response):
    """парсит респонс и возвращает массив

    data_list - массив всех данных 
    target_list - массив необходимых нам данных

    """
    pattern = re.search('APP_INITIALIZATION_STATE=(.*?);window.APP_FLAGS',
                        response.replace('\n', ''))
    if pattern:
    	try:
            data_list = json.loads(pattern.group(1))
            target_list = json.loads(data_list[3][6][4:])
            return target_list[0][1][0][14]					
    	except ValueError:
    		print('Invalid JSON')
    		return None
    else:
        return None

def getaddress(data):
    """Возвращает адрес как строку или None"""
    address = ', '.join(data[2])
    print(address)
    return address   

def getphone(data):
    """Возвращает контактный телефон или None

    data[3] - может быть массивом либо None

    """
    phone = data[3]
    if phone:
        phone1 = phone[0]
        return phone1
    else:
        return None

def getsite(data):
    """Возвращает словарь используемых сайтов 

    data[7] - может быть массивом либо None

    """
    sites = data[7]
    if sites:
        return dict(site1=sites[0],site2=sites[1])
    else:
    	return dict(site1=None,site2=None)

def getgps(data):
    """ Возвращает словарь с координатами

    data[9] - может быть массивом либо None

    """
    gps = data[9]
    if gps:
        return dict(latitude=gps[2],longtitude=gps[3])
    else:
        return dict(latitude=None,longtitude=None)

def getfullname(data):
    """ Возвращает полное название как строку или None"""
    fname = data[11]
    return fname

def gettype(data):
    """ Возвращает строку категорий объекта или None

    разделитель категорий ';'

    """
    cat = data[13]
    if cat:
        return '; '.join(data[13])
    else:
        return None

def getshortrewievs(data):
    """ Возвращает массив коротких отзывов или None

    data[31] - может быть массивом или None

    """
    result = None
    srewievs = data[31]
    if srewievs:
        result = [srewievs[1][x][1] for x in range(0, len(srewievs))]
    return result

def getevents(data):
    """ Вовзращает словарь ближайших мероприятий или None

    data[41] - может быть массивом либо None

    """
    result = []
    events = data[41]
    if events:
        for row in events[0]:
            name = row[0]
            date = todate(row[1])
            link = row[5][0]
            result.append(dict(name=name,date=date,link=link))
        return result
    else:
        return None

def getfullrewievs(data):
    """Принимает массив или None, возвращает словарь отзывов или None

    name - имя автора отзыва
    profilelink - ссылка на g+ профиль автора (если есть)
    text - отзыв
    rating - оценка автора

    """
    result = []
    frewievs = data[52]
    if frewievs and frewievs[0]:
        for row in frewievs[0]:
            name = row[0][1]
            profilelink = row[0][0]
            text = row[3]
            rating = row[4]
            result.append(dict(author_name=name, profilelink=profilelink, rewievtext=text, rating=rating))
        return result
    else:
        return None

def getphoto(data):
    """ Возвращает строку со ссылкой на фотографию или None"""
    photo = data[72]
    if photo:
        return photo[0][0][0]
    else:
        return None

def getworktime(data):
    """ Возвращает массив с часами работы по дням недели или None

    wtime[1] - массив дней недели и времени работы заведения, где
    row[0] - день недели
    row[5] - статус заведения в день недели 1 - открыто, 2 - закрыто
    row[6][0] - массив времени работы ([17, 0, 22, 0] (17:00 - 22:00)) 

    """
    result = []
    
    wtime = data[34]
    if wtime:
        for row in wtime[1]:
            day = {}
            day_of_week = row[0]
            day['day_of_week'] = day_of_week
            isopen = row[5]
            if isopen == 2:
                day['isopen'] = False
            else:
                day['isopen'] = True
                starthour, startminute, closehour, closeminute = row[6][0][0:4]
                day['start_hour'] = starthour
                day['start_minute'] = startminute
                day['close_hour'] = closehour
                day['close_minute'] = closeminute
            result.append(day)
    return result

def parsebyplaceid(proxy=None):
    """ Возвращает массив словарей"""
    with io.open(settings.PID_LIST_FILE, 'r', encoding='utf-8') as file:
        pidl = file.readlines()
    Place.objects.all().delete()  
    for pid in pidl:
        if len(pid.strip()) != 27:
            continue
        time.sleep(3)
        response = makerequest(pid, proxy)
        if response:
            data = parseresponse(response)
            if data:
                dictargs = {}
                address = getaddress(data)
                phone = getphone(data)
                site = getsite(data)
                gps = getgps(data)
                fname = getfullname(data)
                category = gettype(data)
                srewievs = getshortrewievs(data)
                events = getevents(data)
                frewievs = getfullrewievs(data)
                photo = getphoto(data)
                worktime = getworktime(data)
                error = 0
                dictargs = site
                dictargs.update(gps)
                print(pid,address,photo,category,fname,error,dictargs)
                place = Place(pid=pid,address=address,photo=photo,category=category,
                              fullname=fname,error=error,phone=phone,**dictargs)
                place.save()
                if srewievs:
                    for x in srewievs:
                        short_rewievs = ShortRewievs(srewiewtext=x,place=place)
                        short_rewievs.save()
                if events:
                    for x in events:
                        event = Event(place=place,**x)
                        event.save()
                if frewievs:
                    for x in frewievs:
                        full_rewievs = FullRewievs(place=place,**x)
                        full_rewievs.save()
                if worktime:
                    for x in worktime:
                        work_time = WorkTime(place=place,**x)
                        work_time.save()
            else:
                place = Place(pid=pid)
                place.save()

if __name__ == '__main__':
    print('Place ID parser module')

