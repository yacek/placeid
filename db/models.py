from django.db import models

class Place(models.Model):
    pid = models.CharField(primary_key=True,max_length=27,blank=False,unique=True)
    photo = models.TextField(blank=True,null=True)
    address = models.TextField(blank=True,null=True)
    category = models.TextField(blank=True,null=True)
    fullname = models.CharField(max_length=200,blank=True,null=True)
    site = models.TextField(blank=True,null=True)
    phone = models.CharField(max_length=15,blank=True,null=True)
    error = models.IntegerField(default=1)

    def __str__(self):
        return ' - '.join([self.pid,self.fullname])

class ShortRewievs(models.Model):
    srewiewtext = models.TextField(blank=True,null=True)
    place = models.ForeignKey(Place,on_delete=models.CASCADE)

    def __str__(self):
        return self.srewiewtext

class Event(models.Model):
    name = models.CharField(max_length=255,blank=False,null=True)
    date = models.DateTimeField(blank=True,null=True)
    link = models.CharField(max_length=255,blank=True,null=True)
    place = models.ForeignKey(Place,on_delete=models.CASCADE)

    def __str__(self):
        return ', '.join([x for x in (self.name,self.date,self.link) if x])

class FullRewievs(models.Model):
    author_name = models.CharField(max_length=255,blank=True,null=True)
    rewievtext = models.TextField(blank=True,null=True)
    place = models.ForeignKey(Place,on_delete=models.CASCADE)

    def __str__(self):
        return self.rewievtext

class WorkTime(models.Model):
    Monday = models.CharField(max_length=12,blank=True,null=True)
    Tuesday = models.CharField(max_length=12,blank=True,null=True)
    Wednesday = models.CharField(max_length=12,blank=True,null=True)
    Thursday = models.CharField(max_length=12,blank=True,null=True)
    Friday = models.CharField(max_length=12,blank=True,null=True)
    Saturday = models.CharField(max_length=12,blank=True,null=True)
    Sunday = models.CharField(max_length=12,blank=True,null=True)
    place = models.ForeignKey(Place,on_delete=models.CASCADE)

    def __str__(self):
        return ', '.join([self.Monday,self.Tuesday,self.Wednesday,
                         self.Thursday,self.Friday,self.Saturday,self.Sunday])
