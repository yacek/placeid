from django.core.management.base import BaseCommand
import os
import sys 

from db import utilsv2


class Command(BaseCommand):
	help = 'Parse places by place ID, source for IDs pids.txt'
	def add_arguments(self,parser):
		parser.add_argument('--proxy',dest='proxy',type=str,default=None,help='proxy - address:port')

	def handle(self, *args, **options):
		proxy = options['proxy'] or None
		utilsv2.parsebyplaceid(proxy)
		self.stdout.write('Done')
